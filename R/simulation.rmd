

```{r setup, message=FALSE, echo=FALSE}
# packages
library(lubridate)
library(tidyverse)
library(rvle)
library(rsunflo)
library(knitr)

# path (! wd is changed in next chunk)
opts_knit$set(root.dir = rprojroot::find_rstudio_root_file())
opts_chunk$set(echo=FALSE, warning=FALSE, message=FALSE)
```


```{r simulation}

# define network to simulate (GWA, NAM)
network <- "NAM"

# define crop model 
sunflo <- new("Rvle", file = "sunflo_web.vpz", pkg = "sunflo")

# define DOE
design <- read_rds(glue::glue("data/design/design_{network}.rds"))

# multi-simulation
run_sunflo <- function(uid){design %>% play(unit=uid) %>% shape(view="timed")}

list_simulation <- design %>% select(uid)
output <- list_simulation %>% plyr::mdply(possibly(run_sunflo, NULL))

output_timed <- output %>%
  rename(date=time) %>% 
  mutate(date=ymd(date)) %>%
  left_join(design %>% select(uid, id, genotype)) 

# compute indicators 
output_indicators <- output_timed %>%
  group_by(uid, id, genotype) %>%
  do(indicate(.)) %>% ungroup()

output_indicators_phase <- output_timed %>%
  group_by(uid, id, genotype) %>%
  do(indicate(., integration="phase")) %>% ungroup()

# export
write_rds(output_timed, glue::glue("data/simulation/output_timed_{network}.rds"), compress = "gz")
write_csv(output_indicators, glue::glue("data/simulation/output_indicators_{network}.csv"))
write_csv(output_indicators_phase, glue::glue("data/simulation/output_indicators_phase_{network}.csv"))

```


